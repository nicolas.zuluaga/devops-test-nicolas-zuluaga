variable "aws_access_key" {}
variable "aws_secret_key" {}

variable "tags" {
  description = "informations tags"
  type        = map(string)
}
variable "ec2_spec" {
  description = "parametro de instancias"
  type        = map(string)

}