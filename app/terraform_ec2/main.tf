resource "aws_instance" "RedAcreTest" {
  ami             = var.ec2_spec.ami
  instance_type   = var.ec2_spec.instance_type
  security_groups = [aws_security_group.RedAcreTest_sg.name]
  key_name        = "redacre"
  tags = {
    Name = "RedAcre_test"
  }
  user_data = <<-EOF
              #!/bin/bash
              cd /
              mkdir /app
              sudo chmod 777 app
              cd /app
              sudo git clone https://github.com/nicozulux/redacretest.git
              sudo apt-get update
              sudo apt-get install -y docker.io
              sudo apt-get install docker-compose -y
              cd redacretest
              sudo docker-compose up -d
              EOF

}

resource "aws_security_group" "RedAcreTest_sg" {
  name = "security-group"
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

}
