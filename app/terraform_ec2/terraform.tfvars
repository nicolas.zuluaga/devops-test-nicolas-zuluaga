aws_access_key = "XXXXX"
aws_secret_key = "XXXXXXXXX"

tags = {
  "name"     = "RedAcreTest"
  "env"      = "dev"
  "owner"    = "NicolasZuluaga"
  "provider" = "aws"
}

ec2_spec = {
  "ami"         = "ami-053b0d53c279acc90"
  instance_type = "t2.micro"
}