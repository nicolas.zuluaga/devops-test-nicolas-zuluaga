variable "aws_access_key" {}
variable "aws_secret_key" {}

variable "tags" {
  description = "informations tags"
  type        = map(string)
}
