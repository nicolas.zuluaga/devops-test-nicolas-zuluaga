
resource "aws_ecs_cluster" "redacre_cluster" {
  name = "redacre-cluster"
}

resource "aws_ecs_task_definition" "redacre_task" {
  family                   = "redacre-task"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  execution_role_arn       = aws_iam_role.ecs_execution_role.arn
  cpu = "256"
  memory = "512"

  container_definitions = jsonencode([
    {
      name  = "backend",
      image = "./backend:latest",
      portMappings = [
        {
          containerPort = 5000,
          hostPort      = 5000,
        },
      ],
      memory = 512,
    },
    {
      name  = "frontend",
      image = "./frontend:latest",
      portMappings = [
        {
          containerPort = 3000,
          hostPort      = 3000,
        },
      ],
      memory = 512,
    },
  ])
}

resource "aws_iam_role" "ecs_execution_role" {
  name = "ecs-execution-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "ecs-tasks.amazonaws.com",
        },
      },
    ],
  })
}
