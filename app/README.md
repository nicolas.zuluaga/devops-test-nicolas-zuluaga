####First Point
First, I develop the DockerFiles, with this it is possible to create the containers and the app. 
app/backend/Dockerfile
app/frontend/Dockerfile
app/nginx/nginx.conf
app/docker-compose.yml
only need to stay in the directory "/app" and execute "sudo docker-compose up -d"

#### Second Point
Second, I created the option in AWS but with EC2. That's options stay in:
.../app/terraform_ec2/*
only need to stay in the directory "/app/terraform_ec2" and execute "terraform apply" it's possible to take 20 a 25 minutes for the installation process. 

I tried to create ECS, but the task did not run. The code in Terraform connects with AWS but at the moment the task does not start. hower the code it's in the directory "../app/terraform_ECS" 

#### Third point

For this task, I don't have a good laptop. For that reason, on my personal laptop no is possible to run Kubernetes, and I can't check 
"/app/Kubernetes/"

Thanks for the time and the opportunity 
